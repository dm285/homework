using NUnit.Framework;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using System;
using Microsoft.AspNetCore.Mvc;

namespace NUnitTesting
{
	public class Tests
	{
		const string User1 = "451533d5-d8d5-4a11-9c7b-eb9f14e1a32f";
		const string Role1 = "53729686-a368-4eeb-8bfa-cc69b6050d02";
		

		[SetUp]
		public void Setup()
		{
		}

		[Test]
		public async Task Test_FullListIsWorking()
		{
			var eRepo = new InMemoryRepository<Employee>(Otus.Teaching.PromoCodeFactory.DataAccess.Data.FakeDataFactory.Employees);
			var rRepo = new InMemoryRepository<Role>(Otus.Teaching.PromoCodeFactory.DataAccess.Data.FakeDataFactory.Roles);
			var eController = new EmployeesController(eRepo,rRepo);

			var result = await eController.GetEmployeesAsync();

			Assert.That(result, Is.Not.Null);
			Assert.That(result, Is.Not.Empty);
			Assert.That(result, Is.InstanceOf<System.Collections.Generic.List<EmployeeShortResponse>>());
			Assert.IsAssignableFrom<System.Collections.Generic.List<EmployeeShortResponse>>(result);
			Assert.AreEqual(2, result.Count());

			Assert.Pass();
		}
		[Test]
		public async Task Test_GetUserByIDIsWorking()
		{
			var eRepo = new InMemoryRepository<Employee>(Otus.Teaching.PromoCodeFactory.DataAccess.Data.FakeDataFactory.Employees);
			var rRepo = new InMemoryRepository<Role>(Otus.Teaching.PromoCodeFactory.DataAccess.Data.FakeDataFactory.Roles);
			var eController = new EmployeesController(eRepo,rRepo);

			Guid newGuid = Guid.Parse(User1);
			var result = await eController.GetEmployeeByIdAsync(newGuid);

			Assert.That(result, Is.Not.Null);
//			Assert.That(result, Is.Not.Empty);
			Assert.That(result, Is.InstanceOf<ActionResult<EmployeeResponse>>());
			Assert.IsAssignableFrom<ActionResult<EmployeeResponse>>(result);
			

			Assert.Pass();
		}

		[Test]
		public async Task Test_CreateUserIsWorking()
		{
			var eRepo = new InMemoryRepository<Employee>(Otus.Teaching.PromoCodeFactory.DataAccess.Data.FakeDataFactory.Employees);
			var rRepo = new InMemoryRepository<Role>(Otus.Teaching.PromoCodeFactory.DataAccess.Data.FakeDataFactory.Roles);
			var eController = new EmployeesController(eRepo, rRepo);

			var employee = new NonCreatedEmployee();
			employee.AppliedPromocodesCount = 10;
			employee.FirstName = "Ffff";
			employee.LastName = "Lllll";
			employee.Email = "Eeee";
			employee.Role = "Admin";

			var result = await eController.PostAsync(employee);

			Assert.That(result, Is.InstanceOf<bool>());
			Assert.That(result);

			Assert.Pass();
		}

		[Test]
		public async Task Test_DeleteUserIsWorking()
		{
			var eRepo = new InMemoryRepository<Employee>(Otus.Teaching.PromoCodeFactory.DataAccess.Data.FakeDataFactory.Employees);
			var rRepo = new InMemoryRepository<Role>(Otus.Teaching.PromoCodeFactory.DataAccess.Data.FakeDataFactory.Roles);
			var eController = new EmployeesController(eRepo, rRepo);

			Guid newGuid = Guid.Parse(User1);
			var result = await eController.DeleteEmployeeByIdAsync(newGuid);

			Assert.That(result, Is.InstanceOf<bool>());
			Assert.That(result);

			var count = await eController.GetEmployeesAsync();

			Assert.That(count, Is.Not.Null);
			Assert.That(count, Is.Not.Empty);
			Assert.That(count, Is.InstanceOf<System.Collections.Generic.List<EmployeeShortResponse>>());
			Assert.IsAssignableFrom<System.Collections.Generic.List<EmployeeShortResponse>>(count);
			Assert.AreEqual(1, count.Count());

			Assert.Pass();
		}

	}
}