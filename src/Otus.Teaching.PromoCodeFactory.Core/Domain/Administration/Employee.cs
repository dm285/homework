﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        public string Email { get; set; }
        public List<Role> Roles { get; set; }
        public int AppliedPromocodesCount { get; set; }

    }

    public class NonCreatedEmployee
	{
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public int AppliedPromocodesCount { get; set; } = 0;
    }
}