using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<bool> CreateRecordAsync(T data)
        {
            int count = Data.Count();
            Data = Data.Append(data);
            return Task.FromResult(Data.Count()-count==1);
        }

        public Task<bool> DeleteByIdAsync(Guid id)
        {
            int count = Data.Count();
            if (count==0)
                return Task.FromResult(false);
            Data = Data.Where(x => x.Id != id);
            return (count-Data.Count()==1)
        }

        public Task<bool> EditByIdAsync(T data)
        {
           var tList = Data.ToList();            
            int num = tList.IndexOf(tList.FirstOrDefault(x => x.Id == data.Id));
            if (num < 0)
                return Task.FromResult(false);
            tList[num] = data;
            Data = tList;
			return Task.FromResult(true);
        }
    }
}
