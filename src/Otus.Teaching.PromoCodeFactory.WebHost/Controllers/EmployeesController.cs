﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
//        private readonly IRepository<Employee> _employeeRepository;
        private IRepository<Employee> _employeeRepository;
        private IRepository<Role> _rolesRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns>список сотрудников в краткой форме</returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns>Данные сотрудника в полной форме</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Удалить сотрудника по Id
        /// </summary>
        /// <returns>True/False по результату удаления</returns>
        [HttpDelete("{id:guid}")]
        public async Task<bool> DeleteEmployeeByIdAsync(Guid id)
        {
			var res = (bool)await _employeeRepository.DeleteByIdAsync(id);
            return res;
        }

        /// <summary>
        /// Создать нового сотрудника, роль можно указать одну, она будет найдена по названию
        /// </summary>
        /// <returns>True/False по результату создания</returns>
        [HttpPost]
        public async Task<bool> PostAsync([FromBody] NonCreatedEmployee value)
        {
            var newEmployee = new Employee();
            newEmployee.FirstName = value.FirstName;
            newEmployee.LastName = value.LastName;
            newEmployee.Email = value.Email;
            var rolesList = await _rolesRepository.GetAllAsync();
            newEmployee.Roles = rolesList.Where(x => x.Name == value.Role).ToList(); ;
            newEmployee.AppliedPromocodesCount = value.AppliedPromocodesCount;
            newEmployee.Id = Guid.NewGuid();
            var res = await _employeeRepository.CreateRecordAsync(newEmployee);
            return res;
        }

        /// <summary>
        /// Исправить сотрудника
        /// </summary>
        /// <returns>True/False по результату</returns>
        [HttpPut]
        public async Task<bool> PutAsynk(
            string id,
            string FirstName,
            string LastName,
            string Email,
            string Role,
            int AppliedPromocodesCount
            )
		{
			Employee newEmployee = new Employee
			{
				FirstName = FirstName,
				LastName = LastName,
				Email = Email
			};
			var rolesList = await _rolesRepository.GetAllAsync();
            newEmployee.Roles = rolesList.Where(x => x.Name == Role).ToList(); ;
            newEmployee.AppliedPromocodesCount = AppliedPromocodesCount;
            newEmployee.Id = Guid.Parse(id);

			bool res = await _employeeRepository.EditByIdAsync(newEmployee);
            return res;
		}
    }
}